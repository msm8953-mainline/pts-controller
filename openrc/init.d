#!/sbin/openrc-run

# Symlink this init file to launch the correct instance
# ln -s controller controller.pinephone

device="${SVCNAME#*.}"
name="PTS Controller ($device)"
description="PTS device controller managing $device"

BASEDIR="/srv/pts-controller"
VENV="/srv/pts-controller/venv/bin"
configfile="/etc/controller.ini"
logfile="/var/log/pts/$device.log"

command="$VENV"/python3
command_args="-m pts_controller --debug --config '${configfile}' ${device}"
command_background="yes"
command_user="root:root"
pidfile="/run/$RC_SVCNAME.pid"
supervisor=supervise-daemon
supervise_daemon_args="--chdir '$BASEDIR' --stderr='$logfile' --stdout='$logfile'"
required_files="${configfile}"

depend() {
	need net
}

start_pre() {

    if [ "$RC_SVCNAME" = "controller" ]; then
                ewarn "You are not supposed to run this runscript directly. Instead, you should"
                ewarn "create a symlink for the device you want to run the controller instance"
                ewarn "for with a dot and then the config section name, like so:"
                ewarn ""
                ewarn "    ln -s controller /etc/init.d/controller.pinephone"
                ewarn ""
                return 1
    fi

	checkpath -d --owner "$command_user" --mode 0644 "/var/log/pts"
	checkpath -f --owner "$command_user" --mode 0644 "$logfile"
}
