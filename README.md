# PTS Controller

This is the daemon that facilitates communication with the web ui and talks to the actual test hardware.

## Installation

From source:

```shell-session
$ pip install -r requirements.txt
$ python3 -m pts_controller --config /path/to/configfile.ini devicename
```

## Configuration

The controller is configured using an ini config file.

```ini
[general]
# The URL to the instance of PTS CentralController
url = http://127.0.0.1:5000

# Secret generated in the webinterface [Admin -> Controllers -> Add controller]
secret = 006df1fd-44ef-4ca8-a0e7-fd637099b8c8

# One section per device in the test system. The section name is used in the
# commandline for launching the controller instance
[pinephone]
# ID of the device in the webinterface [Admin -> Device]
id = 1

# USB port number the phone is plugged into [use lsplug]
device_port = 1-3

# USB port number the control board is plugged into
control_port = 1-4

# Flashing method, towboot or fastboot
flasher = towboot

# Power control, can be external or pmic.
# pmic makes the controller hold the power button
# to force a reset which is slower and unreliable
power = pmic
```

## Funding

This project was funded through the [NGI0 PET](https://nlnet.nl/PET) Fund, a fund established by [NLnet](https://nlnet.nl/) with
financial support from the European Commission's [Next Generation Internet](https://ngi.eu/) programme, under the aegis of
DG Communications Networks, Content and Technology under grant agreement No 825310.
