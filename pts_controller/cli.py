import glob
import os
import json
import urllib.parse

import click
import requests_unixsocket
import tabulate


def get_controllers():
    result = []
    for socket in glob.glob('/run/pts-controller/ipc-*'):
        pid = int(socket.split('-')[-1])

        try:
            # Send signal 0 to the found pid, this is a no-op but will raise OSError when the process doesn't exist
            os.kill(pid, 0)
        except OSError:
            continue

        name = socket.split('/')[-1]
        name = name.split('-', maxsplit=1)[1]
        name = '-'.join(name.split('-')[0:-1])
        result.append((socket, name))

    return result


@click.command()
def list():
    session = requests_unixsocket.Session()
    table = []
    for socket, name in get_controllers():
        url = 'http+unix://' + urllib.parse.quote_plus(socket) + '/status'
        result = session.get(url)
        info = result.json()
        table.append((info['device'], info['device_id'], info['status'], info['usb']['control'], info['usb']['uart']))
    print(tabulate.tabulate(table, headers=['Device', '#', 'Status', 'Control port', 'UART port']))


@click.command()
@click.argument('script')
def run(script):
    pass


@click.group()
def main():
    pass


main.add_command(list)
main.add_command(run)

if __name__ == '__main__':
    main()
