import subprocess


class Bootloader:
    def __init__(self, config):
        self.config = config

    def reboot(self):
        pass

    def reboot_bootloader(self):
        pass

    def reboot_recovery(self):
        pass

    def format_partition(self, name):
        pass

    def write_partition(self, name, image):
        pass

    def boot(self, image):
        pass

    def continue_boot(self):
        pass


class Fastboot(Bootloader):
    def __init__(self, config):
        super().__init__(config)
        self.serial = config['serial']

    def run(self, cmd):
        cmd = ['fastboot', '-s', self.serial] + cmd
        subprocess.run(cmd)

    def reboot(self):
        self.run(['reboot'])

    def reboot_bootloader(self):
        self.run(['reboot', 'bootloader'])

    def format_partition(self, name):
        self.run(['erase', name])

    def write_partition(self, name, image):
        self.run(['flash', name, image])

    def boot(self, image):
        self.run(['boot', image])

    def continue_boot(self):
        self.run(['continue'])
