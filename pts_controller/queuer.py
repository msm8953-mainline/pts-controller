import glob
import io
import json
import logging
import os.path
import queue
import ssl
import subprocess
import tempfile
import threading
import time
import atexit
from urllib.parse import urlparse, urljoin

import paho.mqtt.client as mqtt
import requests
import serial

from pts_controller import configuration, apiclient, state
import act.parser as act
from act.act_ast import ActBlock
import pts_controller.block as blocks

client = None
q = queue.Queue()

logger = logging.getLogger('queuer')


def update_device_status(status):
    logger.info(f'Updating device status to {status}')
    state.status = status.decode()
    ret = client.publish(_join_topic(configuration.mqtt_prefix, configuration.status_topic), status,
                         retain=True,
                         qos=1)
    ret.wait_for_publish()
    if ret.rc != 0:
        logger.error(f'Could not message mqtt: {ret.rc}')


def get_task():
    resp = apiclient.get(f'/api/v1/device/{configuration.device_id}/get-task')
    if resp.status_code == 200:
        payload = resp.json()
        if 'job' in payload:
            success = run_task(payload)
            if success:
                logger.info('Task was completed sucessfully')
                log_task_state(True)
            else:
                logger.warning('Task has failed')
                log_task_state(False)

            # Power off device after task
            logger.debug('Powering off device')
            state.control.write(b'pbr')
            update_device_status(b'idle')
        else:
            logger.info('There was no task available')
            update_device_status(b'idle')
            if state.control:
                if configuration.flasher == 'fastboot':
                    state.keys.reset_lk2nd()
                else:
                    state.keys.reset(full=True)
    else:
        logger.error(f'Got failure from the HTTP api: {resp.status_code}')


def log_line(channel, message, error=False):
    state.mqtt_client.publish(state.task_log_topic, json.dumps({
        's': state.task_log_section,
        'l': message,
        'c': channel,
        'e': error,
    }))


def log_task_state(result):
    state.mqtt_client.publish(state.task_log_topic, json.dumps({
        's': state.task_log_section,
        'r': result,
    }))


def run_task(task):
    logger.info(f'Getting task details for task {task["task"]}')
    resp = apiclient.post(f'/api/v1/device/{configuration.device_id}/start-task/{task["task"]}')
    if resp.status_code != 200:
        logger.error(f'Got unexpected status from the API: {resp.status_code}')
        return

    update_device_status(b'running')
    state.task_id = task['task']

    if state.task_id == state.cancel_id:
        return False

    payload = resp.json()
    log_topic = payload['topic']

    # Default starting environment
    state.variables = {
        'CI': 'true',
        'CI_JOB': task['job'],
        'CI_TASK': task['task'],
    }

    # Give the other threads a place for logging for this task
    state.mqtt_client = client
    state.task_log_section = 'startup'
    state.task_log_topic = log_topic

    logger.info('Task initializing')
    logger.info(f'Log topic is {state.task_log_topic}')
    log_line('controller', 'Controller initializing')

    # Create a temporary folder for the task files and fetch all artifacts
    tempdir = tempfile.TemporaryDirectory()
    for name in task['artifacts']:
        log_line('controller', f'Fetching artifact "{name}"...')
        response = requests.get(task['artifacts'][name])
        target = os.path.join(tempdir.name, name)
        with open(target, 'wb') as handle:
            handle.write(response.content)

    if state.uart is None:
        logger.info('Connecting to the serial ports')
        # Connect to the serial ports
        logger.debug(f'Connecting to {state.uart_port} at 115200 baud for uart logs')
        state.uart = serial.Serial(state.uart_port, 115200)

    if state.task_id == state.cancel_id:
        return False

    # Disable all gpios
    logger.info('Resetting hardware state...')
    if configuration.flasher == 'fastboot':
        state.keys.reset_lk2nd()
    else:
        state.keys.reset(full=True)
    time.sleep(1)

    # Start manifest execution
    script = act.parse(task['manifest'] + "\n")
    state.variables.update(task['variables'])

    # Loop through the toplevel blocks and execute
    for step in script.contents:
        if state.task_id == state.cancel_id:
            return False
        if isinstance(step, ActBlock):
            logger.info(f'=== {step.name.title()} ===')
            blocktype = step.name.title() + 'Block'
            if hasattr(blocks, blocktype):
                instance = getattr(blocks, blocktype)(step, logger, log_line, tempdir.name)
                success = instance.run()
                if success:
                    logger.debug(f'result of {step.name} is success')
                else:
                    logger.error(f'The {step.name} block has exited with a failure')
                    log_line('controller', f'The {step.name} block has exited with a failure', error=True)
                    return False
            else:
                logger.error(f'Unknown block "{step.name}", can\'t find {blocktype}')
                log_line('controller', f'Unknown block "{step.name}", can\'t find {blocktype}', error=True)

    logger.info('Reached end of manifest')
    return True


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info('Connected to the mqtt broker')
    else:
        logger.error(f'Could not connect to the mqtt broker: return code {rc}')
        return

    # Check if there was a task pending when launching
    q.put(True)

    logger.debug(f'Subscribing to the task topic: {configuration.tasks_topic}')
    client.subscribe(configuration.tasks_topic)


def on_message(client, userdata, msg):
    logger.info(f'Got message: {msg.payload}')
    if msg.payload == b'new-task':
        q.put(True)
    elif msg.payload.startswith(b'cancel-task:'):
        payload = msg.payload.decode()
        _, taskid = payload.split(':', maxsplit=1)
        state.cancel_id = int(taskid)


def queue_thread():
    global client
    client = mqtt.Client()

    c = urlparse(configuration.mqtt_uri, scheme='mqtt')
    if c.username:
        client.username_pw_set(c.username, c.password)

    default_port = 1883
    if c.scheme == 'mqtts':
        context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_2)
        client.tls_set_context(context)
        default_port = 8883

    port = c.port if c.port else default_port

    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(c.hostname, port=port)

    # Set offline on crash
    client.will_set(configuration.status_topic, b'offline', retain=True)

    # Start networking thread for mqtt
    client.loop_start()

    if state.uart is None:
        logger.info('Connecting to the serial ports')
        # Connect to the serial ports
        logger.debug(f'Connecting to {state.uart_port} at 115200 baud for uart logs')
        state.uart = serial.Serial(state.uart_port, 115200, timeout=1)

    # Block on tasks
    while True:
        q.get(block=True)
        get_task()


def _join_topic(self, *args):
    slash = '/' if args[0].startswith('/') else ''
    parts = []
    for p in args:
        parts.append(p.strip('/'))
    return slash + '/'.join(parts).strip('/')


def send_quit_message():
    update_device_status(b'offline')


atexit.register(send_quit_message)
