import os.path
import posixpath
import re
import urllib.parse

import requests


def to_directory(url, targetdir):
    with requests.get(url) as r:

        # Raise exception on http errors
        if not r.status_code == 200:
            r.raise_for_status()

        # Find the best filename for this URL
        if 'Content-Disposition' in r.headers:
            filename = re.findall("filename=(.+)", r.headers["Content-Disposition"])[0]
        else:
            path = urllib.parse.urlsplit(url).path
            filename = posixpath.basename(path)
        if '?' in filename:
            filename, _ = filename.split('?', maxsplit=1)

        target = os.path.join(targetdir, filename)

        # Stream the file contents
        with open(target, 'wb') as fd:
            for chunk in r.iter_content(chunk_size=128):
                fd.write(chunk)

        return target
