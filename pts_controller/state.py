import logging
import time
from time import sleep

from pts_controller import configuration


class KeyState:
    def __init__(self):
        self.power = False
        self.boot = False
        self.supply = False

        self.powered = True

    def reset(self, full=False):
        self.power = False
        self.boot = False
        self.supply = False
        control.write(b'b')
        control.flush()
        control.write(b'r')
        control.flush()
        control.write(b'p')
        control.flush()

        if configuration.power == 'pmic' and full:
            logging.debug("Doing a full pmic hard reset")
            self.press_power(True)
            sleep(1)
            self.press_power(False)
            sleep(1)
            self.press_power(True)
            sleep(18)
            self.press_power(False)
            self.powered = False
        elif configuration.power == 'pmic' and self.powered:
            logging.debug("Doing a quick pmic hard reset")
            self.press_power(True)
            time.sleep(18)
            self.press_power(False)

    def press_power(self, state):
        self.power = state
        control.write(b'B' if state else b'b')
        control.flush()

    def press_bootloader(self, state):
        self.boot = state
        control.write(b'R' if state else b'r')
        control.flush()

    def set_power(self, state):
        control.write(b'P' if state else b'p')

        # Device does not have external power control, shut down by holding the power key
        if configuration.power == 'pmic' and not state and self.powered:
            self.press_power(True)
            sleep(18)
            self.press_power(False)
        self.supply = state
        if state:
            self.powered = state
        control.flush()

    def reset_lk2nd(self):
        self.power = False
        self.boot = False
        self.supply = True
        control.write(b'b')
        control.flush()
        control.write(b'r')
        control.flush()
        control.write(b'P')
        control.flush()

        logging.debug('Hard reset (15 seconds)')

        self.press_power(True)

        while True:
            time.sleep(1)
            line = uart.readline()
            if line.startswith(b'\x00\x00Android Bootloader - UART_DM Initialized!!!'):
                logging.debug('Entered aboot')
                self.press_power(False)
                break

        logging.debug('Hard reset completed, starting device...')
        uart.flushInput()

        # para entrar en lk2nd
        time.sleep(1)
        self.press_bootloader(True)

        while True:
            line = uart.readline()
            if line.startswith(b'Android Bootloader - UART_DM Initialized!!!'):
                logging.debug('Entered lk2nd')
                self.press_bootloader(False)
                return True

        return False

mqtt_client = None
task_log_topic = None
task_log_section = None

control_port = None
uart_port = None

control = None
uart = None
keys = KeyState()
status = 'idle'

variables = {}
task_id = None
cancel_id = None
