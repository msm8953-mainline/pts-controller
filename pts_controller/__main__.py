import glob
import http.server
import json
import logging
import os.path
import threading
import time
from functools import partial
from urllib.parse import urlparse, parse_qsl, urljoin

import requests as requests

import pts_controller.state as state
from pts_controller import configuration, apiclient
from pts_controller.model import UsbDevice
from pts_controller.queuer import queue_thread
from pts_controller.udev import udev_thread
from pts_controller.unixhttp import UnixSocketHttpServer

ipcfile = None


class PTSLogFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    white = "\x1b[97;20m"
    blue = "\x1b[36;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = '[%(levelname)-8s %(threadName)-12s] %(name)-14s - %(message)s'

    FORMATS = {
        logging.DEBUG: blue + format + reset,
        logging.INFO: white + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


class IPCHandler(http.server.BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def do_GET(self):
        print("GET")
        path = self.path
        if path == '/':
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write('Controller IPC API'.encode())
            return

        parts = urlparse(path)
        path = parts.path[1:]
        parts = path.split('/')
        command = parts[0]

        result = {}
        status = 200
        if command == 'status':
            result['device'] = configuration.device
            result['device_id'] = configuration.device_id
            result['usb'] = {
                'control': state.control_port,
                'uart': state.uart_port,
                'device': configuration.device_port,
            }
            result['status'] = state.status
        else:
            result['error'] = 'unknown command'
            status = 404
        self.send_response(status)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        print(result)
        res = json.dumps(result)
        self.wfile.write(res.encode())

    def do_POST(self):
        print("POST")
        path = self.path
        if path == '/':
            print('400')
            self.send_response(400)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write('Magma controller IPC API'.encode())
            return

        parts = urlparse(path)
        path = parts.path[1:]
        parts = path.split('/')
        command = parts[0]
        length = int(self.headers['Content-length'])
        raw = self.rfile.read(length).decode()
        payload = json.loads(raw)
        print(payload)

        if command == 'submit':
            script = payload['script']
            #run_script(script)


def ipc_thread():
    server = UnixSocketHttpServer((ipcfile), IPCHandler)
    logging.info(f'Started IPC server on {ipcfile}')
    with server:
        server.serve_forever()


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', '-c', default='/etc/pts/controller.ini')
    parser.add_argument('--verbose', action='store_true', help='Show more log messages')
    parser.add_argument('--debug', action='store_true', help='Display a lot of debugging info')
    parser.add_argument('device', help='Device to manage')
    args = parser.parse_args()

    ch = logging.StreamHandler()
    ch.setFormatter(PTSLogFormatter())

    if args.verbose:
        logging.basicConfig(level=logging.INFO, handlers=[ch])
    elif args.debug:
        logging.basicConfig(level=logging.DEBUG, handlers=[ch])
    else:
        logging.basicConfig(handlers=[ch])
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    logging.info(f"Loading config file '{args.config}'")
    configuration.load_config(args.config, args.device)

    logging.info("Register IPC channel")
    global ipcfile
    ipcdir = '/run/pts-controller'
    if not os.path.isdir(ipcdir):
        os.makedirs(ipcdir)
    ipcfile = os.path.join(ipcdir, f'ipc-{args.device}')
    for file in glob.glob(ipcfile + '-*'):
        exist_pid = int(file.split('-')[-1])
        try:
            # Send signal 0 to the found pid, this is a no-op but will raise OSError when the process doesn't exist
            os.kill(exist_pid, 0)
        except OSError:
            # Clean up old pidfile
            os.unlink(file)
            continue

        logging.fatal(f"A controller is already running for device '{args.device}'. Found PID {exist_pid}")
        exit(1)

    ipcfile += f'-{os.getpid()}'

    logging.info('Get initial setup data from central controller...')
    resp = apiclient.get('/api/v1/controller/handshake', payload={
        'device': configuration.device_id
    })

    if resp.status_code != 200:
        if resp.status_code == 302 and '/login' in resp.headers.get('Location'):
            logging.fatal('Invalid authentication secret for the controller')
        elif resp.status_code == 404:
            logging.fatal('The requested device number does not exit')
        elif resp.status_code == 403:
            logging.fatal('The requested device number is not associated with this controller')
        else:
            logging.fatal(resp.content)
        exit(1)
    payload = resp.json()
    configuration.controller = payload['id']
    configuration.mqtt_uri = payload['mqtt_uri']
    configuration.device = args.device
    configuration.status_topic = payload['status_topic']
    configuration.tasks_topic = payload['tasks_topic']
    logging.info(f'MQTT server:  {configuration.mqtt_uri}')
    logging.debug(f'Status topic: {configuration.status_topic}')
    logging.debug(f'Tasks topic:  {configuration.tasks_topic}')

    ipc = threading.Thread(target=ipc_thread, name='IPC')
    ipc.daemon = True
    ipc.start()

    ut = threading.Thread(target=udev_thread, name='Udev')
    ut.daemon = True
    ut.start()

    # Let udev settle
    time.sleep(1)

    # Contact the central controller for initial setup
    qt = threading.Thread(target=queue_thread, name='Queue')
    qt.daemon = True
    qt.start()

    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
