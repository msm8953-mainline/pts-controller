import glob
import logging
import os
import subprocess
import threading
import time

from pts_controller import configuration, state

logger = logging.getLogger('flasher')


class UMSFlasher(threading.Thread):
    def __init__(self, source, log):
        threading.Thread.__init__(self, name='flasher')
        self.source = source
        self.log = log

    def find_target(self):
        for device in glob.glob('/sys/bus/usb/devices/*/host*'):
            part = device.split('/')
            port_intf = part[-2]
            port, intf = port_intf.split(':', maxsplit=1)
            if port != configuration.device_port:
                continue

            disks = list(glob.glob(os.path.join(device, 'target*', '*', 'block', '*')))
            for disk in disks:
                blockdev = disk.split('/')[-1]
                path = os.path.join('/dev', blockdev)
                return path
        return None

    def run(self):
        self.log('flasher', f'Searching for disk on USB port {configuration.device_port}')
        while True:
            target = self.find_target()
            if target is not None:
                break
            time.sleep(0.2)
        self.log('flasher', f'Found disk {target}')
        self.log('flasher', f'Writing image to disk...')
        logger.info(f'running bmaptool to flash {self.source} to {target}')
        subprocess.run(['sudo', 'bmaptool', 'copy', '--nobmap', self.source, target])
        logger.info('syncing disk')
        self.log('flasher', f'Writing image completed, syncing...')
        subprocess.run(['sudo', 'sync'])
        self.log('flasher', f'Triggering reboot...')
        logger.info('Trigger reboot after flashing')
        state.keys.reset()
        time.sleep(1)
        state.keys.set_power(True)

class FastbootFlasher(threading.Thread):
    def __init__(self, source, log):
        threading.Thread.__init__(self, name='flasher')
        self.source = source
        self.log = log

    def find_target(self):
        for device in glob.glob('/sys/bus/usb/devices/*/host*'):
            part = device.split('/')
            port_intf = part[-2]
            port, intf = port_intf.split(':', maxsplit=1)
            if port != configuration.device_port:
                continue

            disks = list(glob.glob(os.path.join(device, 'target*', '*', 'block', '*')))
            for disk in disks:
                blockdev = disk.split('/')[-1]
                path = os.path.join('/dev', blockdev)
                return path
        return None

    def run(self):
        self.log('flasher', f'Searching for disk on USB port {configuration.device_port}')
        while True:
            target = self.find_target()
            if target is not None:
                break
            time.sleep(0.2)
        self.log('flasher', f'Found disk {target}')
        self.log('flasher', f'Writing image to disk...')
        logger.info(f'running bmaptool to flash {self.source} to {target}')
        # subprocess.run(['sudo', 'fastboot', 'flash', 'userdata', self.source, target])
        logger.info('syncing disk')
        self.log('flasher', f'Writing image completed, syncing...')
        subprocess.run(['sudo', 'sync'])
        self.log('flasher', f'Triggering reboot...')
        logger.info('Trigger reboot after flashing')
        # state.keys.reset()
        time.sleep(1)
        # state.keys.set_power(True)
