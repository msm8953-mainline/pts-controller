import configparser
import logging
import os

url = None
secret = None

mqtt_uri = None
controller = None
mqtt_prefix = None
device = None

status_topic = None
tasks_topic = None

device_id = None
device_port = None
control_port = None
uart_port = None
flasher = None
power = None


def load_config(path, devicename):
    if not os.path.isfile(path):
        raise RuntimeError(f"Configuration not found: {path}")
    parser = configparser.ConfigParser()
    parser.read(path)

    global url, secret
    url = parser.get('general', 'url')
    secret = parser.get('general', 'secret')

    if not parser.has_section(devicename):
        logging.fatal(f'Device "{devicename}" does not exist in the config file')
        exit(1)

    global device_id, device_port, control_port, uart_port, flasher, power

    device_id = int(parser.get(devicename, 'id'))
    device_port = parser.get(devicename, 'device_port')
    control_port = parser.get(devicename, 'control_port')
    uart_port = parser.get(devicename, 'uart_port', fallback=None)
    flasher = parser.get(devicename, 'flasher')
    power = parser.get(devicename, 'power')
