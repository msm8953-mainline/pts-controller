from pts_controller.bootloader import Bootloader


class UsbDevice:
    def __init__(self):
        self.port = None
        self.devpath = None
        self.device = None
        self.connection = None

        self.product = None
        self.vendor = None
        self.model = None
        self.serial = None

        self.node = None
        self.is_fastboot = False

    def __repr__(self):
        return f'<UsbDevice {self.connection} {self.node} {self.vendor} {self.model} {self.serial} @ {self.port}>'


class Device:
    def __init__(self):
        self.name = None
        self.state = 'unknown'
        self.usb_port = None
        self.uart_port = None

        self.bootloader_type = None
        self.bootloader_serial = None

        self.power_type = "sysrq"

        self.bootloader = Bootloader([])
        self.serial = None

    def transition(self, state):
        if state == self.state:
            print("No state change needed")
            return

        if self.state == 'bootloader':
            if state == 'shell':
                self.bootloader.continue_boot()
