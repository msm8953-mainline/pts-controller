import glob
import json
import logging
import os.path

import serial

import pts_controller.state as state

import pyudev

from pts_controller import configuration
from pts_controller.model import UsbDevice

logger = logging.getLogger('udev')


def usb_device_add(device):
    port = device.device_path.split('/')[-1]

    if port not in [configuration.device_port, configuration.control_port, configuration.uart_port]:
        logger.debug(f'New USB device on untracked port {port}, ignoring')
        return

    logger.debug(f'New USB connection on port {port}')

    portname = None
    if port == configuration.control_port:
        portname = 'control'
    elif port == configuration.device_port:
        portname = 'device'
    elif port == configuration.uart_port:
        portname = 'uart'

    ud = UsbDevice()
    ud.device = portname
    ud.port = port
    ud.connection = device.properties.device.device_node
    ud.devpath = device.get('DEVPATH')
    ud.product = device.get('PRODUCT')
    ud.vendor = device.get('ID_VENDOR')
    ud.model = device.get('ID_MODEL')
    ud.serial = device.get('ID_SERIAL')

    logger.info(f"New USB device: {ud.connection} on the {portname} port")

    if portname == 'device':
        # Send to the task log if a task is running
        if state.task_log_topic is not None:
            state.mqtt_client.publish(state.task_log_topic, json.dumps({
                's': state.task_log_section,
                'ui': usb_device_to_description(ud.devpath)
            }))
    elif portname == 'control':
        # Find all serial ports on the control USB port
        for interface in glob.glob(os.path.join('/sys', ud.devpath[1:], '*:*/tty*')):
            part = interface.split('/')
            ttydir = part[-1]
            if ttydir == 'tty':
                subdir = list(glob.glob(os.path.join(interface, '*')))[0]
                sdp = subdir.split('/')[-1]
                tty = os.path.join('/dev', sdp)
            else:
                tty = os.path.join('/dev', ttydir)

            name = sysfs_str(os.path.join(interface, '../interface'))
            logger.debug(f'Interface {interface} is {tty} ({name})')

            if name == "Control":
                logger.info(f'Control port: {tty}')
                state.control_port = tty
                logger.debug(f'Connecting to {state.control_port} at 115200 baud for control')
                state.control = serial.Serial(state.control_port, 115200)
            elif name == "Passthrough":
                logger.info(f'UART port: {tty}')
                state.uart_port = tty
            else:
                logger.info(f'Control port: {tty}')
                state.control_port = tty


def sysfs_str(path):
    if os.path.isfile(path):
        with open(path, 'r') as handle:
            return handle.read().strip()
    else:
        return None


def sysfs_int(path):
    result = sysfs_str(path)
    if result is None:
        return None
    return int(result)


def usb_device_to_description(path):
    path = os.path.join('/sys', path.lstrip('/'))
    result = {
        'vid': sysfs_str(os.path.join(path, 'idVendor')),
        'pid': sysfs_str(os.path.join(path, 'idProduct')),
        'serial': sysfs_str(os.path.join(path, 'serial')),
        'manufacturer': sysfs_str(os.path.join(path, 'manufacturer')),
        'product': sysfs_str(os.path.join(path, 'product')),
        'speed': sysfs_int(os.path.join(path, 'speed')),
        'device_class': sysfs_str(os.path.join(path, 'bDeviceClass')),
        'device_subclass': sysfs_str(os.path.join(path, 'bDeviceSubClass')),
        'device_protocol': sysfs_str(os.path.join(path, 'bDeviceProtocol')),
        'interfaces': []
    }

    for intf in glob.glob(os.path.join(path, '*:*')):
        interface = {
            'number': sysfs_int(os.path.join(intf, 'bInterfaceNumber')),
            'interface': sysfs_str(os.path.join(intf, 'interface')),
            'interface_class': sysfs_str(os.path.join(intf, 'bInterfaceClass')),
            'interface_subclass': sysfs_str(os.path.join(intf, 'bInterfaceSubClass')),
            'interface_protocol': sysfs_str(os.path.join(intf, 'bInterfaceProtocol')),
        }
        result['interfaces'].append(interface)

    return result


def usb_device_remove(device):
    port = device.device_path.split('/')[-1]

    portname = None
    if port == configuration.control_port:
        portname = 'control'
    elif port == configuration.device_port:
        portname = 'device'
    elif port == configuration.uart_port:
        portname = 'uart'

    if port not in [configuration.device_port, configuration.control_port, configuration.uart_port]:
        logger.debug(f'USB device unplugged from untracked port {port}')
        return
    connection = device.properties.device.device_node
    logger.info(f"USB device {connection} unplugged from {portname} port {port}")

    if port == configuration.device_port:
        # Send to the task log if a task is running
        if state.task_log_topic is not None:
            state.mqtt_client.publish(state.task_log_topic, json.dumps({
                's': state.task_log_section,
                'uo': port
            }))


def udev_thread():
    global defined_ports
    context = pyudev.Context()

    # Get initial list of usb devices from udev
    for device in context.list_devices(subsystem='usb', DEVTYPE='usb_device'):
        usb_device_add(device)

    # Start monitoring for hot-plug events
    monitor = pyudev.Monitor.from_netlink(context)
    for device in iter(monitor.poll, None):
        if device.subsystem == 'usb' and device.get('DEVTYPE') == 'usb_device':
            if device.action == 'add':
                usb_device_add(device)
            elif device.action == 'remove':
                usb_device_remove(device)


if __name__ == '__main__':
    udev_thread()
