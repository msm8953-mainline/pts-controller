from urllib.parse import urljoin
import logging

import requests

from pts_controller import configuration

logger = logging.getLogger('api')


def get(url, payload=None):
    return request('get', url, payload)


def post(url, payload=None):
    return request('post', url, payload)


def request(method, url, payload=None):
    logger.debug(f'{method.upper()} {url}')
    kwargs = {}
    if payload is not None:
        kwargs['json'] = payload
    return requests.request(method, urljoin(configuration.url, url), headers={
        'Authorization': f'Bearer {configuration.secret}'
    }, allow_redirects=False, **kwargs)
