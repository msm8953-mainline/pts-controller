import os.path
import queue
import shlex

import requests.exceptions

from pts_controller import state, configuration, fetch
from act.act_ast import ActStatement, ActBlock
from pts_controller.flasher import UMSFlasher, FastbootFlasher


class Block:
    def __init__(self, block, controller_log, task_log, tempdir):
        self.data = block.data
        self.block = block
        self.controller_log = controller_log
        self.task_log = task_log
        self.tempdir = tempdir

    def run_command(self):
        pass

    def log(self, component, message, error=False):
        self.task_log(component, message, error=error)
        if error:
            self.controller_log.error(message)
        else:
            self.controller_log.info(message)

    def info(self, message):
        self.controller_log.info(message)

    def debug(self, message):
        self.controller_log.debug(message)

    def readline(self, timeout=None):
        state.uart.timeout = timeout
        line = state.uart.readline()
        try:
            line = line.decode()
        except:
            # There might be corrupt bytes that don't decode cleanly dump it as literals
            line = repr(line)
        return line


class EnvBlock(Block):
    def run(self):
        for key in self.data:
            state.variables[key] = self.data[key]
        return True


class BootBlock(Block):
    def run(self):
        files = ['bootimg', 'rootfs']
        has_file = False
        file = {}
        for f in files:
            if f in self.data:

                filename = self.data[f]
                for key in state.variables:
                    filename = filename.replace('${' + key + '}', str(state.variables[key]))

                if filename.startswith('http://') or filename.startswith('https://'):
                    # Remote file
                    self.log('boot', f'Fetching {f} "{filename}"...')
                    try:
                        target = fetch.to_directory(filename, self.tempdir)
                    except requests.exceptions.RequestException as e:
                        self.log('boot', f'Could not fetch {f} from "{filename}": {e}', error=True)
                        return False
                    file[f] = target
                else:
                    # Local file
                    if not os.path.isfile(os.path.join(self.tempdir, filename)):
                        self.log('boot', f'File "{filename}" for "{f}" does not exist', error=True)
                        return False
                    file[f] = os.path.join(self.tempdir, filename)
                has_file = True
        if not has_file:
            self.log('boot', 'No image to flash specified', error=True)
            return False

        if configuration.flasher == 'towboot':
            if 'rootfs' not in file:
                self.log('boot', 'Tow-Boot devices require a rootfs image', error=True)
                return False
            return self.flash_towboot(file['rootfs'])
        elif configuration.flasher == 'fastboot':
            if 'rootfs' not in file:
                self.log('boot', 'Fastbiit devices require a rootfs image', error=True)
                return False
            return self.flash_lk2nd(file['rootfs'])

        return False

    def flash_lk2nd(self, target):
        self.log('controller', 'rebooting to lk2nd')
        status = state.reset_lk2nd()

        if status == False:
            self.log('controller', 'error rebooting to lk2nd')
            return status

        flashing = True
        flasher = FastbootFlasher();

    def flash_towboot(self, target):
        self.log('controller', 'powering up the hardware')
        state.keys.reset()
        state.keys.set_power(True)
        flashing = True

        while True:
            line = self.readline()
            if state.task_id == state.cancel_id:
                return False
            if line.startswith('U-Boot SPL '):
                self.info('Entered SPL')
                if state.task_log_section == 'spl':
                    state.keys.set_power(False)
                    self.log('controller', 'Boot loop detected, stopping job', error=True)
                    return False
                state.task_log_section = 'spl'

                # Release power button if it was held down
                if state.keys.power:
                    state.keys.press_power(False)

                # Hold bootloader button if flashing is needed
                if flashing:
                    state.keys.press_bootloader(True)

            elif line.startswith('Tow-Boot '):
                self.info('Entered Tow-Boot')
                state.task_log_section = 'tow-boot'
            elif (state.task_log_section == 'tow-boot' and line.startswith('Starting kernel')) or line.startswith(
                    '[    0.000000] Booting Linux on physical CPU'):
                state.task_log_section = 'kernel'
                self.info('Entered Kernel')
                if flashing:
                    self.log('controller',
                             'Kernel started while the flashing mode should\'ve been started. Flashing failed',
                             error=True)
                    return False
                else:
                    # Flashing completed, go to next block
                    self.log('controller', 'Flashing completed sucessfully')
                    return True
            elif state.task_log_section == 'tow-boot' and line.startswith('Allwinner mUSB OTG'):
                if not flashing:
                    self.log('controller',
                             'Mass storage entered while not trying to flash, aborting',
                             error=True)
                    return False

                state.task_log_section = 'usb mass storage'
                self.info('Entered UMS')
                flashing = False
                self.log('controller', 'Mass storage detected, starting flasher')
                thread = UMSFlasher(target, self.log)
                thread.daemon = True
                thread.start()
            elif state.task_log_section == 'tow-boot' and 'poweroff ...' in line:
                self.log('controller',
                         'Flashed image was not bootable',
                         error=True)
                return False

            if state.task_log_section == 'usb mass storage':
                # The annoying spinner animation uses \r line endings only
                line = line.split('\r', maxsplit=1)[0]

            if state.task_log_section == 'usb mass storage' and len(line) < 5:
                # Don't log the spinner animation
                continue

            self.log('uart', line.rstrip())
        return True


class PowerBlock(Block):
    def run(self):
        pass


class FastbootBlock(Block):
    def run(self):
        for command in self.block.contents:
            if isinstance(command, ActStatement):
                cmd = ['fastboot', '-s', self.context.device.fastboot_serial]
                ucmd = shlex.split(command.statement)
                for i in range(0, len(ucmd)):
                    ucmd[i] = self.context.interpolate(ucmd[i])
                self.run_command(cmd + ucmd)


class ShellBlock(Block):
    def run(self):
        username = self.data.get('username', None)
        password = self.data.get('password', None)
        prompt = self.data.get('prompt', '/ #').encode()
        environment = self.data.get('env', 'yes') == 'yes'
        success = self.data.get('success', None)
        fail = self.data.get('fail', "PTS-FAIL")
        wrap = self.data.get('wrap', 'yes') == 'yes'

        commands = queue.Queue()
        if environment:
            for key in state.variables:
                commands.put(f'export {key}={state.variables[key]}')

        for item in self.block.contents:
            if isinstance(item, ActBlock) and item.name == "script":
                for line in item.contents:
                    if isinstance(line, ActStatement):
                        if wrap:
                            commands.put(line.statement + f' || echo "{fail}"')
                        else:
                            commands.put(line.statement)

        logged_in = False
        login_wait = 1000
        login_timeouts = 0
        script_timeouts = 0
        state.uart.timeout = 10
        while True:
            is_timeout = False
            is_prompt = False
            buffer = b''
            while True:
                char = state.uart.read(1)
                if char == b'':
                    is_timeout = True
                    break
                buffer += char

                # TODO: Implement login detect here

                if buffer == prompt:
                    is_prompt = True
                    if commands.empty():
                        # If no sucess checker is defined and no commands remain, mark as task as success
                        if success is not None:
                            self.log('controller',
                                     'Reached end of script and success condition was not found',
                                     error=True)
                        return success is None
                    command = commands.get() + "\n"
                    state.uart.write(command.encode())
                if char == b'\n':
                    break
            line = buffer.decode()

            if success is not None and not is_prompt and success in line:
                self.log('uart', line)
                self.log('controller',
                         'Success condition found')
                return True
            if not is_prompt and fail in line:
                self.log('uart', line, error=True)
                self.log('controller',
                         'Fail condition found', error=True)
                return False

            if state.task_id == state.cancel_id:
                return False

            if not logged_in:
                if is_timeout:
                    login_timeouts += 1
                    if login_timeouts > (60 / state.uart.timeout):
                        self.log('controller',
                                 'Timeout while waiting on a login prompt',
                                 error=True)
                        return False
                else:
                    login_timeouts = 0
                login_wait -= 1
                if login_wait == 0:
                    self.log('controller',
                             'Timeout while waiting on a login prompt',
                             error=True)
                    return False
            else:
                if is_timeout:
                    script_timeouts += 1
                    if script_timeouts > (120 / state.uart.timeout):
                        self.log('controller',
                                 'Timeout while waiting on uart data',
                                 error=True)
                        return False
            if is_timeout:
                continue
            self.log('uart', line)


class TelnetBlock(Block):
    def run(self):
        pass
